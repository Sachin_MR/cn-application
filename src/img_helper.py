"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import numpy as np
import cv2


def _draw_bounding_boxes_tiny(frame, boxes, scores, classes, names, img_shape):
    """
    Draw bounding boxes and labels on an image.

    Args:
        frame (numpy.ndarray): The image on which bounding boxes and labels will be drawn.
        boxes (list): List of bounding boxes in the format [x_min, y_min, x_max, y_max].
        scores (list): Confidence scores for each detected object.
        classes (list): List of class labels for each detected object.
        names (list): List of class names corresponding to the labels.
        img_shape (tuple): Shape of the original image (height, width).

    Returns:
        numpy.ndarray: The input image with bounding boxes and labels drawn on it.
    """
    class_i = 0
    for bbox in boxes:
        text = names[int(classes[class_i])].title()
        color_codes = [[255, 128, 63], [71, 217, 5], [
            199, 35, 0], [157, 7, 248], [255, 255, 0]]
        (x, y), base = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 0.8, 2)
        xmin = (bbox[0]/img_shape[1])*frame.shape[1]
        ymin = (bbox[1]/img_shape[0])*frame.shape[0]
        xmax = (bbox[2]/img_shape[1])*frame.shape[1]
        ymax = (bbox[3]/img_shape[0])*frame.shape[0]
        xmin, ymin, xmax, ymax = int(xmin), int(ymin), int(xmax), int(ymax)
        frame = cv2.rectangle(frame, (xmin, ymin - y - base),
                              (xmin + x+20, ymin+10), color_codes[classes[class_i]], -1)
        cv2.rectangle(frame, (xmin, ymin), (xmax, ymax),
                      color_codes[classes[class_i]], 2)
        frame = cv2.putText(frame, text, (xmin+10, ymin),
                            cv2.FONT_HERSHEY_DUPLEX, 0.8, (0, 0, 0), 2, cv2.LINE_AA)
        class_i += 1
    return frame


def postprocess_img(img, img_size, bond_boxes=None):
    """
    Postprocess an image, resizing and adjusting bounding boxes accordingly.

    Args:
        img (numpy.ndarray): The input image to be postprocessed.
        img_size (tuple): Target size of the image (height, width).
        bond_boxes (numpy.ndarray or None): Bounding boxes to be adjusted. If None, only resizing is performed.

    Returns:
        numpy.ndarray: The resized image.
        numpy.ndarray or None: Adjusted bounding boxes or None if bond_boxes is None.
    """

    img_h, img_w = img.shape[:2]
    width, height = img_size

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2

    img = img[dh:new_h + dh, dw:new_w + dw, :]
    img_resized = cv2.resize(img, (width, height))

    if bond_boxes is None:
        return img_resized, None
    else:
        bond_boxes = bond_boxes.astype(np.float32)
        bond_boxes[:, [0, 2]] = np.clip(
            (bond_boxes[:, [0, 2]] - dw) / img_scale, 0., width)
        bond_boxes[:, [1, 3]] = np.clip(
            (bond_boxes[:, [1, 3]] - dh) / img_scale, 0., height)

        return img_resized, bond_boxes


def preprocess_img(img, img_size, bond_boxes=None):
    """
    Preprocess an image, resizing and adjusting bounding boxes accordingly.

    Args:
        img (numpy.ndarray): The input image to be preprocessed.
        img_size (tuple): Target size of the image (width, height).
        bond_boxes (list or None): Bounding boxes to be adjusted. If None, only resizing is performed.

    Returns:
        numpy.ndarray: The preprocessed and padded image.
        numpy.ndarray or None: Adjusted bounding boxes or None if bond_boxes is None.
    """

    img_w, img_h = img_size
    height, width, _ = img.shape

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    img_resized = cv2.resize(img, (new_w, new_h))

    img_paded = np.full(shape=[1, img_h, img_w, 3],
                        dtype=np.uint8, fill_value=127)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2
    img_paded[:, dh:new_h + dh, dw:new_w + dw, :] = img_resized

    if bond_boxes is None:
        return img_paded

    else:
        bond_boxes = np.asarray(bond_boxes).astype(np.float32)
        bond_boxes[:, [0, 2]] = bond_boxes[:, [0, 2]] * img_scale + dw
        bond_boxes[:, [1, 3]] = bond_boxes[:, [1, 3]] * img_scale + dh

        return img_paded, bond_boxes


def decode_class_names(classes_path):
    """
    Decode class names from a file.

    Args:
        classes_path (str): Path to the file containing class names, one per line.

    Returns:
        list: List of class names.
    """
    with open(classes_path, 'r') as f:
        lines = f.readlines()
    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)
    return classes
