# Use a base image
FROM arm64v8/ubuntu:18.04

# Use another base image
FROM python:3.7-slim-buster

# Set environment variable to non-interactive mode
ARG DEBIAN_FRONTEND=noninteractive


# Update package repositories
RUN apt-get update

# Install required libraries
RUN apt install -y libgl1-mesa-glx
RUN apt install -y libglib2.0-0
RUN apt install -y gcc
RUN apt-get install -y ffmpeg

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install opencv-python
RUN python3 -m pip install requests
RUN python3 -m pip install flask
RUN python3 -m pip install flask_ngrok
RUN python3 -m pip install grpcio grpcio-tools
RUN python3 -m pip install backports.zoneinfo
RUN python3 -m pip install boto3
RUN python3 -m pip install awsiot awsiotsdk

COPY static/ static/
COPY model_data/ model_data/
COPY config config/
COPY templates/ templates/

COPY src src/
COPY device_certs/ device_certs/

# Set working directory for gRPC files
WORKDIR /src/grpc_files

CMD python gen.py

# Reset working directory
WORKDIR /

COPY main.py main.py

# Set the entrypoint to run 'main.py' script
ENTRYPOINT [ "python3" ,"main.py" ]
